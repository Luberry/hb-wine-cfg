# Hit Box Designer Wine Configuration

Linux installation files for Hit Box controller designer programs. 

***NOTE: this is not an officially supported way of using the designer so YMMV***

## Requirements
* Linux system running `wine`. Follow the instructions for your linux distro [here](https://wiki.winehq.org/Download). 
* `winetricks` - can be installed folowing the instructions [here](https://wiki.winehq.org/Winetricks)
* CH340 Linux drivers ( `Cross|Up`, `Gen 1 Smash Box` or `Gen 3 Smash Box` )
  * Sparkfun has a nice tutorial [here]()https://learn.sparkfun.com/tutorials/how-to-install-ch340-drivers/all#linux
  
## Installation
To install the designer run the following command. You may replace the placeholder prefix `hit_box` with your own if you wish.
This will install dotnet and the designer, as well as create a desktop shortcut for each. If you previously had a designer installed, please uninstall it
or use a fresh wineprefix, unless you don't care about the desktop shortcut being wonky.
***NOTE: it may fail the first time saying its not compatible with your wine version, it will work if you run with `--force`***
```sh
# Cross|Up
winetricks prefix=hit_box ./crossup_designer.verb

# Smash Box: make sure to uncheck all checkboxes in the installer.
winetricks prefix=hit_box ./smash_box_designer.verb
```

## Running
Make sure your controller is plugged in with the side programming USB before launching the program.
Then just launch the program from the shortcut. Your controller's COM port will most likely be mapped
to one of the highest number ports in the com port menu. You should be able to use the designer as
you normally would now. 
